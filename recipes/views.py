from django.shortcuts import render, get_object_or_404, redirect
from recipes.models import Recipe
from recipes.forms import RecipeForm
from django.contrib.auth.decorators import login_required
# from django.contrib.auth.decorators import login_required

# Create your views here.

@login_required
def create_recipe(request):
    if request.method == "POST": #  if the form has been submitted
        form = RecipeForm(request.POST) # a form bound to the POST data
        if form.is_valid(): # if all validation rules pass
            form.save() # 3save the new recipe to the database ---------may need to add back
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()

            return redirect("recipe_list") # redirect after POST
    else: # else if the form has not been submitted
        form = RecipeForm() # return the same form back to the user
    context = { # build the context dictionary to pass to the template
        "form": form,
    }
    return render(request, "recipes/create.html", context) # render the template

def show_recipe(request,id):
    recipe = get_object_or_404(Recipe, id=id)
    context = {
        "recipe_object": recipe,
    }
    return render(request, "recipes/detail.html", context)

def recipe_list(request):
    recipes = Recipe.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)

def edit_recipe(request, id):
    recipe = get_object_or_404(Recipe, id=id)
    if request.method == "POST":
        form = RecipeForm(request.POST, instance=recipe)
        if form.is_valid():
            form.save()
            return redirect("show_recipe", id=id)
    else:
        form = RecipeForm(instance=recipe)
    context = {
        "form": form,
        "recipe": recipe,
    }
    return render(request, "recipes/edit.html", context)

def redirect_to_recipe_list(request):
    return redirect("recipe_list")

@login_required
def my_recipe_list(request):
    recipes = Recipe.objects.filter(author=request.user)
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)
